// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore/lite'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB6vpoP6QCdx2KBpoV2tJZuI_4ZmFzhBcM",
  authDomain: "journalapp-backend-5a373.firebaseapp.com",
  projectId: "journalapp-backend-5a373",
  storageBucket: "journalapp-backend-5a373.appspot.com",
  messagingSenderId: "413875690973",
  appId: "1:413875690973:web:ce3ead47433ecda8f4ea34"
};

// Initialize Firebase
export const FirebaseApp = initializeApp(firebaseConfig);
export const FirebaseAuth = getAuth(FirebaseApp);
export const FirebaseDB = getFirestore(FirebaseApp);
